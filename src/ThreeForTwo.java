
public class ThreeForTwo implements DiscountStrategy{
	@Override
	public double getdiscount(Item item, int count) {
		return (item.getPrice() * (int)(count/3));
	}
}
