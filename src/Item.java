public class Item{
	private String name;
	private double price;
	private DiscountStrategy discount;
	public Item(String name, double price) {
		this.name = name;
		this.price = price;
		this.discount = null;
	}
	public Item(String name, double price, DiscountStrategy strat) {
		this.name = name;
		this.price = price;
		this.discount = strat;
	}
	public void setName(String newname) {
		this.name = newname;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getName() {
		return this.name;
	}
	public double getPrice() {
		return this.price;
	}	
	public void setDiscount(DiscountStrategy strat) {
		this.discount = strat;
	}
	public DiscountStrategy getDiscountStrat() {
		return this.discount;
	}
	public double total(int count) {
		if(this.getDiscountStrat()==null) {
			return (this.getPrice() *count);
		}
		else {
			return ((this.getPrice()*count)- (this.getDiscountStrat().getdiscount(this, count))); 
		}
	}
}