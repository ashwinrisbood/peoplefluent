import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cart {
	Map<Item, Integer> cart_map;
	
	public Cart() {
		cart_map = new HashMap<Item, Integer>();
	}
	
	public void addItem(Item t) {
		this.cart_map.put(t, this.cart_map.getOrDefault(t,0)+1);
	}
	
	public void addItem(List<String> ls, Shop shop) {
		for(String s: ls) {
			Item current_item = shop.getRegisteredItems().get(s);
			this.addItem(current_item);
		}
	}
	// this function is unused
	public boolean decrementItem(Item t) {
		if(!this.cart_map.containsKey(t)) return false;
		this.cart_map.put(t, this.cart_map.get(t)-1);
		if(this.cart_map.get(t)==0) {
			this.cart_map.remove(t);
		}
		return true;
	}
	
	public double totalPrice() {
		double total = 0;
		for(Map.Entry<Item, Integer> e: cart_map.entrySet()) {
			total += e.getKey().total(e.getValue());
		}
		return Math.round(total*100.0)/100.0;
	}
}
