import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Shop {
	// Map to translate a String into an Item type
	/*
	 * For the application requirements this contains only Apples and Oranges
	 * and their prices. Functions have been mapped out to enable extensions if required.
	 * 
	 */	
	private Map<String,Item> registered_items;
	// constructor to initialize the Map
	
	public Shop() {
		registered_items = new HashMap<String, Item>();
	}	
	public Map<String, Item> getRegisteredItems() {
		return registered_items;		
	}
	// Simple function to add an item type to the registered_item hashmap
	public void registerItems(Item item) {
		registered_items.put(item.getName(), item);
	}
	public void registerDefault() {
		Item Apple= new Item("Apple", 0.6);
		Item Orange = new Item("Orange", 0.25);
		this.registerItems(Apple);
		this.registerItems(Orange);
		Apple.setDiscount(new Bogo());
		Orange.setDiscount(new ThreeForTwo());
	}
	@SuppressWarnings("resource")
	public List<String> getConsoleInput() {
		List<String> ls = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);
		String inp = "";
		while(true) {
			System.out.println("Enter Item name(0 to exit)");
			inp = sc.nextLine().trim();
			if(inp.trim().equals("0")) break;
			if(!this.registered_items.containsKey(inp)) {
				System.out.println("Item not in the Shop's Catalog!");
				continue;
			}
			ls.add(inp);
		}
		return ls;
	}
	public static void main(String args[]) {
		Shop shop = new Shop();
		Cart cart = new Cart();
		// registering default apples and oranges/
		// the function adds the now offers to these items.
		shop.registerDefault();
		/* If you don't want to use console to add input uncomment the below 2 lines,
		 *  edit the String array, and comment out ls = shop.getConsoleInput line
		 */
		//String[] input = new String[] {"Apple", "Orange", "Apple", "Apple"};
		//List<String> ls = new ArrayList<String>(Arrays.asList(input));
		List<String> ls = shop.getConsoleInput();
		
		// adding items to the cart
		cart.addItem(ls, shop);
		// printing output
		System.out.println(cart.totalPrice());
	}
}
