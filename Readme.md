To run the project please have java environment setup in the system.

##### Steps to Run:
1) git clone https://bitbucket.org/ashwinrisbood/peoplefluent
#### if you want to run the 1st step:-
 - git checkout e37662a
 - cd src
 - javac Shop.java
 - java Shop
 
#### if you want to return to running step 2
 - git checkout bd4e6fd
 - cd src
 - javac Shop.java
 - java Shop
 
####  Thinking process
- 1) create separate class for items, this class holds all functions related to Items, including addition of n items. 
- 2) create separate class for Cart, this holds the map of all items and their count in the cart.
 	also a total function which calls the total on each item(Alernatively could have implemented it as an array of Items, but deletions become costly)
- 3) Shop class to setup the possible items of a shop, get input and trigger the calculations.
#### for step 2,
- the use case was ideal for a strategy design pattern which lets me add a property to a class without changing a lot of its implementation.
- the idea was to add a Interface for discount strategy and adding it to the Items. 